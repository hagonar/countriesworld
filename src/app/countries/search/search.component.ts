import { Component, OnInit } from '@angular/core';
import { GenericDataService } from '../../shared/services/generic-data.service';
import { GLOBAL } from '../../shared/global';
import { Subscription } from 'rxjs';
import { DataSearch } from '../countries-list/countries-list.service';

/**
 * Component responsible for managing the search
 */
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  /**
   * Subscribe to regions
   */
  private subscriptionRegions: Subscription;

  /**
   * Search text
   */
  searchText: string = "";

  /**
   * Option selected from DropDown
   */
  optionSelected: string = "";

  /**
   * Constructor
   * @param genericDataService Generic data service
   */
  constructor(private genericDataService: GenericDataService) { }

  /**
   * Component initialization
   */
  ngOnInit(): void {
    this.subscribes();
  }

  /**
   * Unsubscribe to optimize application performance
   */
  ngOnDestroy(): void {
    this.subscriptionRegions.unsubscribe();
  }

  /**
   * Subscribe to the event to get the regions
   */
  subscribes(): void {
    this.subscriptionRegions = this.genericDataService.getData(GLOBAL.countries.eventEmmit.regionsCountries).subscribe((regions: any[]) => {
      if(regions !== null && regions !== undefined && regions.length > 0) {
        this.genericDataService.setData(GLOBAL.countries.eventEmmit.setOptionsDropDowns, regions);
        this.genericDataService.setData(GLOBAL.countries.eventEmmit.setTextDropDowns, regions[0]);
        this.optionSelected = regions[0];
      }
    });
  }

  /**
   * Emmit event to search for countries from input and button
   */
  searchCountry(): void {
    let dataSearch: DataSearch = {
      term: (this.searchText === null || this.searchText === undefined ? "" : this.searchText),
      filterType: this.optionSelected
    };

    this.genericDataService.setData(GLOBAL.countries.eventEmmit.searchCountry, dataSearch);
  }

  /**
   * Emmit event to search for countries from drop down
   * @param option Search option
   */
  clickOption(option: string): void {
    this.searchText = "";
    this.optionSelected = option;
    this.genericDataService.setData(GLOBAL.countries.eventEmmit.selectCountries, option);
  }
}
