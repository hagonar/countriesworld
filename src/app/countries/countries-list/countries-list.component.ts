import { Component, OnInit } from '@angular/core';
import { GenericDataService } from '../../shared/services/generic-data.service';
import { CountriesListService, DataSearch, ResultDataRegions } from './countries-list.service';
import { Subscription } from 'rxjs';
import { GLOBAL } from '../../shared/global';
import { Region } from '../../countries/countries-list/region.model';
import { Country } from '../../countries/country/country.model';
import { Utility } from '../../shared/utility';

/**
 * Component responsible for listing the countries
 */
@Component({
  selector: 'app-countries-list',
  templateUrl: './countries-list.component.html',
  styleUrls: ['./countries-list.component.css']
})
export class CountriesListComponent implements OnInit {

  /**
   * Subscribe to the country search
   */
  private subscriptionSearch: Subscription;

  /**
   * Subscribe to the country search from DropDown
   */
  private subscriptionSelectCountries: Subscription;

  /**
   * Regions with their countries
   */
  regions: Region[] = [];

  /**
   * Container width of each region
   */
  widthRegions: string = "";

  /**
   * Indicates if a country is selected
   */
  selectedCountry: Country = null;

  /**
   * No results found message
   */
  messageNoFound: string = GLOBAL.messageNoFound;

  /**
   * Error message
   */
  errorMessage: string = "";

  /**
   *Indicates if the application is loading
   */
  loading: boolean = true;

  /**
   * Constructor
   * @param genericDataService Generic data service
   * @param countriesListService Countries list service
   */
  constructor(private genericDataService: GenericDataService, private countriesListService: CountriesListService) { }

  /**
   * Component initialization
   */
  ngOnInit(): void {
    this.getCountries();
    this.subscribes();
  }

  /**
   * Unsubscribe to optimize application performance
   */
  ngOnDestroy(): void {
    this.subscriptionSearch.unsubscribe();
    this.subscriptionSelectCountries.unsubscribe();
  }

  /**
   * Get the list of countries and group them by region
   */
  getCountries(): void {
    this.loading = true;

    this.countriesListService.getAll(GLOBAL.countries.endPoints.allCountries)
      .then((countries: Country[]) => {
        this.loading = false;
        countries = Utility.sort(countries);

        let resultDataRegions: ResultDataRegions = this.countriesListService.setRegions(countries, true, true);
        this.regions = resultDataRegions.regions;
        this.widthRegions = resultDataRegions.widthRegions;

        this.genericDataService.setData(GLOBAL.countries.eventEmmit.allCountries, countries);
      })
      .catch(error => {
        this.loading = false;
        this.errorMessage = error.message;
      });
  }

  /**
   * Subscribe to country search events
   */
  subscribes(): void {
    this.subscriptionSearch = this.genericDataService.getData(GLOBAL.countries.eventEmmit.searchCountry).subscribe((dataSearch: DataSearch) => {
      if(dataSearch !== null && dataSearch !== undefined) {
        this.regions = this.countriesListService.searchCountries(dataSearch).regions;
      }
    });

    this.subscriptionSelectCountries = this.genericDataService.getData(GLOBAL.countries.eventEmmit.selectCountries).subscribe((term: string) => {
      if(term !== null && term !== undefined) {
        this.regions = this.countriesListService.selectCountries(term).regions;
      }
    });
  }

  /**
   * Show a country in a modal
   * @param country Country to be marked as selected
   */
  showCountry(country: Country): void {
    this.selectedCountry = country;
    this.genericDataService.setData(GLOBAL.countries.eventEmmit.showCountry, country);
  }

  /**
   * Hide the modal of the country
   */
  hideCountry(): void {
    this.selectedCountry = null;
  }

  /**
   * Indicate if the country is favorite
   * @param countryCode Country code to search
   * @returns Is it a favorite country or not
   */
  isFavoriteCountry(countryCode: string): boolean {
    return Utility.isFavoriteCountry(countryCode);
  }
}
