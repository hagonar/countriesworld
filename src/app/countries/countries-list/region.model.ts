import { Country } from '../country/country.model';
/**
 * Class to model a region
 */
export class Region {
/**
 * Name of the region
 */
name: string;

/**
 * Countries of the region
 */
countries: Country[];

/**
 * Constructor
 */
constructor(){}
}
