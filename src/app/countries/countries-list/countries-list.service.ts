import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GLOBAL } from '../../shared/global';
import { Country } from '../country/country.model';
import { Utility } from 'src/app/shared/utility';
import { Region } from './region.model';
import { GenericDataService  } from "../../shared/services/generic-data.service";

/**
 * Service that handles the logic related to list of countries
 */
@Injectable({
  providedIn: 'root'
})
export class CountriesListService {

  /**
   * Contains the consulted regions of the api
   */
  private allRegions: Region[] = [];

  /**
   * Contains the consulted countries of the api
   */
  private allCountries: Country[] = [];

  /**
   * Constructor
   * @param http Handle http requests
   * @param genericDataService Generic data service
   */
  constructor(private http: HttpClient, private genericDataService: GenericDataService) { }

  /**
   * Consult the api to obtain all the countries
   * @param param API parameter
   * @returns Countries in a promise
   */
  getAll(param: string): Promise<any[]> {
    return this.http.get<any[]>(`${GLOBAL.countries.urlApi}${param}`).toPromise();
  }

  /**
   * Group the countries by region
   * @param countries Countries to be grouped
   * @param createRegionsCountries Create regions by countries
   * @param createAllRegions Stores all regions and all countries
   * @returns Regions grouped by country
   */
  setRegions(countries: Country[], createRegionsCountries: boolean = false, createAllRegions: boolean = false): ResultDataRegions {
    let countriesRegion: any[] = [];
    let regions: Region[] = [];
    let widthRegions: string = "";

    // Asociar paises a regiones
    countries.forEach((country: Country) => {
      if(countriesRegion[country.region] == null) {
        countriesRegion[country.region] = [];
      }

      countriesRegion[country.region].push(country);
    });

    // Llenar arreglo de paises por regiones
    for(let region in countriesRegion) {
      regions.push({
        name: (countriesRegion[region][0].region ? countriesRegion[region][0].region : GLOBAL.countries.optionsSearch.withoutRegion),
        countries: countriesRegion[region]
      });
    }

    // Ordenar regiones
    regions = Utility.sort(regions);

    if(createRegionsCountries) {
      this.createRegionsCountries(regions);
    }

    if(createAllRegions) {
      this.allRegions = regions;
      this.allCountries = countries;
      widthRegions = `calc((100% / ${this.allRegions.length}) - 20px)`;
    }

    return {regions: regions, widthRegions: widthRegions };
  }

  /**
   * Create regions for the observable regionsCountries
   * @param regions Regions
   */
  private createRegionsCountries(regions: Region[]): void {
    let regionsCountries = [GLOBAL.countries.optionsSearch.showAll, GLOBAL.countries.optionsSearch.favorites];

    regions.forEach((region: Region) => {
      regionsCountries.push(region.name);
    });

    this.genericDataService.setData(GLOBAL.countries.eventEmmit.regionsCountries, regionsCountries);
  }

  /**
   * Get countries by filter type
   * @param filterType Filter type
   * @returns Countries by filter type
   */
  private getCountriesByFilterType(filterType: string): Country[] {
    let countries: Country[] = [];

    switch (filterType) {
      case GLOBAL.countries.optionsSearch.showAll:
      countries = this.allCountries;
      break;

      case GLOBAL.countries.optionsSearch.favorites:
      countries = this.getCountriesFavorites();
      break;

      default:
      countries = this.getCountriesRegions(filterType);
      break;
    }

    return countries;
  }

  /**
   * Get countries favorites
   * @returns Countries favorites
   */
  private getCountriesFavorites(): Country[] {
    let countriesFavorites: Country[] = [];

    this.allCountries.forEach((country: Country) => {
      if(localStorage.getItem(country.alpha3Code) === "true") {
        countriesFavorites.push(country);
      }
    });

    return countriesFavorites;
  }

  /**
   * Get countries in a region
   * @param term Term to search
   * @returns Countries of a region
   */
  private getCountriesRegions(term: string): Country[] {
    let regions: Region[] = [];
    let countries: Country[] = [];

    regions = this.allRegions.filter(region => region.name.toLowerCase() === term.toLowerCase());

    if(regions.length > 0) {
      countries = regions[0].countries;
    }

    return countries;
  }

  /**
   * Search countries
   * @param dataSearch Data to search
   * @returns Regions
   */
  searchCountries(dataSearch: DataSearch): ResultDataRegions {
    return this.setRegions(this.getCountriesByFilterType(dataSearch.filterType).filter(country => country.name.toLowerCase().includes(dataSearch.term.toLowerCase())));
  }

  /**
   * Search countries through the select
   * @param term Country searched
   * @returns Regions
   */
  selectCountries(term: string): ResultDataRegions {
    return this.setRegions(this.getCountriesByFilterType(term));
  }
}

/**
 * Interface to handle the data returned from the setRegions method
 */
export interface ResultDataRegions {
  /**
   * Regions
   */
  regions: Region[];

  /**
   * Container width of each region
   */
  widthRegions?: string,
}

/**
 * Interface to manage the data to be searched
 */
export interface DataSearch {
  /**
   * Term to search
   */
  term: string;

  /**
   * Filter type
   */
  filterType: string
}
