import { Utility } from 'src/app/shared/utility';
import { TestBed } from '@angular/core/testing';
import { CountriesListService, DataSearch } from './countries-list.service';
import { HttpClientModule } from '@angular/common/http';
import { Country } from '../country/country.model';

describe('CountriesListService', () => {
  let service: CountriesListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule]
    });
    service = TestBed.inject(CountriesListService);
  });


  let countries: Country[] = [
    { name: "Armenia", region: "Asia", alpha3Code: "ARM" },
    { name: "Albania", region: "Europe", alpha3Code: "ALB" },
    { name: "Colombia", region: "Americas", alpha3Code: "COL" }
  ];

  it('Set regions', () => {
    expect(service.setRegions(countries).regions[0].name).toEqual("Americas");
    expect(service.setRegions(countries).regions[0].countries[0].name).toEqual("Colombia");
  });

  it('Search countries', () => {
    let dataSearch: DataSearch = {
      term: "col",
      filterType: "Show All"
    };

    service.setRegions(countries, true, true);
    expect(service.searchCountries(dataSearch).regions[0].countries[0].name).toEqual("Colombia");
  });

  it('Select countries', () => {
    service.setRegions(countries, true, true);
    expect(service.selectCountries("Americas").regions[0].countries[0].name).toEqual("Colombia");
    expect(service.selectCountries("Show All").regions[1].countries[0].name).toEqual("Armenia");
  });

  it('Get countries by filter type', () => {
    let countryCode = "COL";

    localStorage.setItem(countryCode, "true");
    service.setRegions(countries, true, true);

    expect(service["getCountriesByFilterType"]("Asia")[0].name).toEqual("Armenia");
    expect(service["getCountriesByFilterType"]("Favorites")[0].name).toEqual("Colombia");
  });

  it('Get favorite country', () => {
    let countryCode = "COL";

    localStorage.setItem(countryCode, "true");
    expect(Utility.isFavoriteCountry(countryCode)).toBeTrue();

    localStorage.setItem(countryCode, "false");
    expect(Utility.isFavoriteCountry(countryCode)).toBeFalse();
  });
});
