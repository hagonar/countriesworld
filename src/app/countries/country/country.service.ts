import { Injectable } from '@angular/core';
import { Country, Currency, Language } from './country.model';

/**
 * Service that handles the logic related to a country
 */
@Injectable({
  providedIn: 'root'
})
export class CountryService {

  /**
   * constructor
   */
  constructor() { }

  /**
   * Get the currencies of a country
   * @param country Country from which the currencies will be obtained
   * @returns Currencies of a country
   */
  getCountryCurrencies(country: Country): string {
    let currencies: string = "";

    country.currencies.forEach((currency: Currency) => {
      currencies += `${currency.name}, `;
    });

    if(currencies !== "") {
      currencies = `${currencies.substring(0, currencies.length -2)}.`;
    }

    return currencies;
  }

  /**
   * Get the languages of a country
   * @param country Country from which the languages will be obtained
   * @returns languages of a country
   */
  getCountryLanguages(country: Country): string {
    let languages: string = "";

    country.languages.forEach((language: Language) => {
      languages += `${language.name}, `;
    });

    if(languages !== "") {
      languages = `${languages.substring(0, languages.length -2)}.`;
    }

    return languages;
  }

  /**
   * Get the border country of a country
   * @param country Country from which the border countries will be obtained
   * @param countries Countries from where the border countries are to be found
   * @returns Border country of a country
   */
  getCountryBorder(country: Country, countries: Country[]): string {
    let borderCountries: string = "";

    country.borders.forEach((borderCountry: string) => {
      let country = countries.find(country => country.alpha3Code === borderCountry);

      if(country !== null && country !== undefined) {
        borderCountries += `${country.name}, `;
      }
    });

    if(borderCountries !== "") {
      borderCountries = `${borderCountries.substring(0, borderCountries.length - 2)}.`;
    }

    return borderCountries;
  }

  /**
   * Save country as favorite
   * @param countryCode Favorite country code
   * @returns Favorite country
   */
  saveFavorite(countryCode: string): boolean {
    let favoriteCountry: boolean = false;

    if(localStorage.getItem(countryCode) === "true") {
      localStorage.setItem(countryCode, "false");
      favoriteCountry = false;
    }
    else {
      localStorage.setItem(countryCode, "true");
      favoriteCountry = true;
    }

    return favoriteCountry;
  }
}
