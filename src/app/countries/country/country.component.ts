import { Component, OnInit } from '@angular/core';
import { GenericDataService } from '../../shared/services/generic-data.service';
import { Country } from './country.model';
import { GLOBAL } from '../../shared/global';
import { Subscription } from 'rxjs';
import {CountryService } from './country.service';
import { Utility } from 'src/app/shared/utility';

/**
 * Component responsible for managing a pai
 */
@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {

  /**
   * Subscribe to all countries
   */
  private subscriptionCounties: Subscription;

  /**
   * Subscribe to a country
   */
  private subscriptionCountry: Subscription;

  /**
   * Country
   */
  country: Country = new Country();

  /**
   * Countries
   */
  countries: Country[] = [];

  /**
   * Indicates if a country is favorite
   */
  favoriteCountry: boolean = false;

  /**
   * Country currencies
   */
  currencies: string = "";

  /**
   * Country languages
   */
  languages: string = "";

  /**
   * Neighbor countries
   */
  borderCountries: string = "";

  /**
   * Constructor
   * @param genericDataService Generic data service
   * @param countryService Country service
   */
  constructor(private genericDataService: GenericDataService, private countryService: CountryService) { }

  /**
   * Component initialization
   */
  ngOnInit(): void {
    this.subscribes();
  }

  /**
   * Unsubscribe to optimize application performance
   */
  ngOnDestroy(): void {
    this.subscriptionCounties.unsubscribe();
    this.subscriptionCountry.unsubscribe();
  }

  /**
   * Subscribe to receive countries and a particular country
   */
  subscribes(): void {
    this.subscriptionCounties = this.genericDataService.getData(GLOBAL.countries.eventEmmit.allCountries).subscribe((countries: Country[]) => {
      if(countries !== null && countries !== undefined) {
        this.countries = countries;
      }
    });

    this.subscriptionCountry = this.genericDataService.getData(GLOBAL.countries.eventEmmit.showCountry).subscribe((country: Country) => {
      if(country !== null && country !== undefined) {
        this.country = country;
        this.favoriteCountry = Utility.isFavoriteCountry(this.country.alpha3Code);
        this.currencies = this.countryService.getCountryCurrencies(this.country);
        this.languages = this.countryService.getCountryLanguages(this.country);
        this.borderCountries = this.countryService.getCountryBorder(this.country, this.countries);
      }
    });
  }

  /**
   * Save country as favorite
   * @param countryCode Country code
   */
  saveFavorite(countryCode: string): void {
    this.favoriteCountry = this.countryService.saveFavorite(countryCode);
  }
}
