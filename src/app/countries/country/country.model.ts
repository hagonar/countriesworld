/**
 * Class to model a country
 */
 export class Country {
  /**
   * Name of the country
   */
  name: string;

  /**
   * Region of the country
   */
  region: string;

  /**
   * Population of the country
   */
  population?: string;

  /**
   * Capital of the country
   */
  capital?: string;

  /**
   * Currencies of the country
   */
  currencies?: Currency[];

  /**
   * Languages of the country
   */
  languages?: Language[];

  /**
   * Borders of the country
   */
  borders?: string[];

  /**
   * Flag of the country
   */
  flag?: string;

   /**
   * Code of the country
   */
  alpha3Code: string;

  /**
   * Constructor
   */
  constructor(){}
 }

 /**
  * Interface to model the currencies of country
  */
export interface Currency {
  /**
   * Currency name
   */
  name: string
}

/**
 * Interface to model the languages of country
 */
export interface Language {
  /**
   * Language name
   */
  name: string
}


