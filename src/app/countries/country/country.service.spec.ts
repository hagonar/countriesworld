import { Utility } from 'src/app/shared/utility';
import { TestBed } from '@angular/core/testing';
import { Country, Currency, Language } from './country.model';
import { CountryService } from './country.service';

describe('CountryService', () => {
  let service: CountryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountryService);
  });

  let currencies: Currency[] = [{ name: "Armenian dram" }];
  let languages: Language[] = [{ name: "Armenian" }, { name: "Russian" }];
  let borders: any[] = ["ALB", "COL", "ARM", "BRA"];
  let countries: Country[] = [
    { name: "Armenia", region: "Asia", currencies: currencies, languages: [], borders: [], alpha3Code: "ARM" },
    { name: "Albania", region: "Europe", currencies: [], languages: languages, borders: [], alpha3Code: "ALB" },
    { name: "Colombia", region: "Americas", currencies: [], languages: [], borders: borders, alpha3Code: "COL" }
  ];

  it('Get favorite country', () => {
    let countryCode = "COL";
    localStorage.setItem(countryCode, "true");

    expect(Utility.isFavoriteCountry(countries[0].alpha3Code)).toBeFalse();
    expect(Utility.isFavoriteCountry(countries[2].alpha3Code)).toBeTrue();
  });

  it('Get country currencies', () => {
    expect(service.getCountryCurrencies(countries[0])).toEqual("Armenian dram.");
    expect(service.getCountryCurrencies(countries[1])).toEqual("");
  });

  it('Get country languages', () => {
    expect(service.getCountryLanguages(countries[0])).toEqual("");
    expect(service.getCountryLanguages(countries[1])).toEqual("Armenian, Russian.");
  });

  it('Get country border', () => {
    expect(service.getCountryBorder(countries[0], countries)).toEqual("");
    expect(service.getCountryBorder(countries[2], countries)).toEqual("Albania, Colombia, Armenia.");
  });

  it('Save favorite', () => {
    let countryCode = "COL";

    localStorage.setItem(countryCode, "true");
    expect(service.saveFavorite(countryCode)).toBeFalse();

    localStorage.setItem(countryCode, "false");
    expect(service.saveFavorite(countryCode)).toBeTrue();

    localStorage.removeItem(countryCode);
    expect(service.saveFavorite(countryCode)).toBeTrue();
  });

});
