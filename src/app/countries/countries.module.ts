import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components
import { HeaderComponent } from '../shared/components/header/header.component';
import { SearchComponent } from './search/search.component';
import { CountriesListComponent } from './countries-list/countries-list.component';
import { CountryComponent } from './country/country.component';
import { LoadingComponent } from '../shared/components/loading/loading.component';
import { DropdownsComponent } from '../shared/components/dropdowns/dropdowns.component';

// Routes
import { CountriesRoutingModule } from './countries-routing.module';


@NgModule({
  declarations: [
    HeaderComponent,
    SearchComponent,
    CountriesListComponent,
    CountryComponent,
    LoadingComponent,
    DropdownsComponent
  ],
  exports:[
    HeaderComponent,
    SearchComponent,
    CountriesListComponent,
    CountryComponent,
    LoadingComponent,
    DropdownsComponent
  ],
  imports: [
    CommonModule,
    CountriesRoutingModule,
    FormsModule
  ]
})
export class CountriesModule { }
