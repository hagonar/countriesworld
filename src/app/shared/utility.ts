/**
 * General utilities of the application
 */
export class Utility {

  /**
   * Constructor
   */
  constructor(){}

  /**
   * Sort an ascending or descending array
   * @param array Array to order
   * @param type Sort order
   * @returns Array sorted
   */
  static sort(array, type = "asc"): any[] {

    if(type === "asc")  {
      return array.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
    }
    else {
      return array.reverse((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
    }
  }

  /**
   * Indicates if a country is favorite
   * @param countryCode Country code to search
   * @returns Is it a favorite country or not
   */
  static isFavoriteCountry(countryCode: string): boolean {
    return (localStorage.getItem(countryCode) === "true" ? true : false);
  }
}
