import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

/**
* Service that allows handling data through subscriptions globally in the application
*/
@Injectable({
  providedIn: 'root'
})
export class GenericDataService {
  /**
   * Array that allows saving received data
   */
  private dataSubject: any[] = [];

  /**
   * It allows obtaining the updated value of a specific data
   * @param key Identifier
   * @returns Observable
   */
  getData(key): Observable<any> {
    if(!this.dataSubject[key]){
      this.dataSubject[key] = new BehaviorSubject(null);
    }
    return this.dataSubject[key].asObservable();
  }

  /**
   * It allows updating the value of a specific position of the dataSubject array
   * @param key Identifier
   * @param data Data to save
   */
  private refreshData(key, data): void {
    if(!this.dataSubject[key]){
      this.dataSubject[key] = new BehaviorSubject(null);
    }
    this.dataSubject[key].next(data);
  }

  /**
  * Allows you to set or modify a value in the dataSubject array
  * @param key Identifier
  * @param data Data to save
  */
  setData(key: string, data: any): void {
    this.refreshData(key, data);
  }
}
