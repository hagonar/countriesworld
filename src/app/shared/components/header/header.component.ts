import { Component, OnInit } from '@angular/core';

/**
 * Component responsible for generating the header
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  /**
   * Constructor
   */
  constructor() { }

  ngOnInit(): void {
  }

}
