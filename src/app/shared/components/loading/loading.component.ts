import { Component, OnInit } from '@angular/core';

/**
 * Component that generates the loading
 */
@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {

  /**
   * Constructor
   */
  constructor() { }

  ngOnInit(): void {
  }

}
