import { Component, OnInit, ViewChild, ElementRef, HostListener, Output, EventEmitter } from '@angular/core';
import { GenericDataService } from '../../services/generic-data.service';
import { GLOBAL } from '../../global';
import { Subscription } from 'rxjs';

/**
 * Component that generates the DropDowns
 */
@Component({
  selector: 'app-dropdowns',
  templateUrl: './dropdowns.component.html',
  styleUrls: ['./dropdowns.component.css']
})
export class DropdownsComponent implements OnInit {

  /**
   * Manage select toogle
   */
  @ViewChild('toggleSelectSearch') toggleSelectSearch: ElementRef;

  /**
   * Event emitted when the user selects an option
   */
  @Output() clickOption = new EventEmitter<string>();

  /**
   * Subscribe to change DropDowns text
   */
  private subscriptionSetOptions: Subscription;

  /**
   * Subscribe to set the DropDowns options
   */
  private subscriptionSetText: Subscription;

  /**
   * Indicates if the select is open
   */
  selectOpen: boolean = false;

  /**
   * Indicates the selected option
   */
  optionSelected: string = "";

  /**
   * Select options
   */
  optionsSelect: string[] = [];

  /**
   * Constructor
   * @param genericDataService Generic data service
   */
  constructor(private genericDataService: GenericDataService) { }

  /**
   * Component initialization
   */
  ngOnInit(): void {
    this.subscribes();
  }

  /**
   * Unsubscribe to optimize application performance
   */
  ngOnDestroy(): void {
    this.subscriptionSetOptions.unsubscribe();
    this.subscriptionSetText.unsubscribe();
  }

  /**
   * Close DropDown when clicked outside of it
   */
  @HostListener("document:click", ["$event"])
  clickOutSideSelect(event): void {
    if(!this.toggleSelectSearch.nativeElement.contains(event.target)) {
      this.selectOpen = false;
    }
  }

  /**
   * Subscribe to options and set text events
   */
  subscribes(): void {
    this.subscriptionSetOptions = this.genericDataService.getData(GLOBAL.countries.eventEmmit.setOptionsDropDowns).subscribe((options: string[]) => {
      if(options !== null && options !== undefined && options.length > 0) {
        this.optionsSelect = options;
      }
    });

    this.subscriptionSetText = this.genericDataService.getData(GLOBAL.countries.eventEmmit.setTextDropDowns).subscribe((value: string) => {
      if(value !== null && value !== undefined) {
        this.optionSelected = value;
      }
    });
  }

  /**
   * DropDown toggle event
   */
  toggleSelect(): void {
    this.selectOpen = !this.selectOpen;
  }

  /**
   * Event that is executed when selecting an option from the DropDown and emit the event clickOption
   * @param option Option selected by the user
   */
  selectOption(option: string): void {
    this.selectOpen = false;
    this.optionSelected = option;
    this.clickOption.emit(this.optionSelected);
  }
}
