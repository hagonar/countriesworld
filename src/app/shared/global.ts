/**
 * Global constants of the application
 */
export const GLOBAL = {
  countries: {
    urlApi: "https://restcountries.eu/rest/v2/",
    endPoints: {
      allCountries: "all"
    },
    eventEmmit: {
      allCountries: "allCountries",
      regionsCountries: "regionsCountries",
      searchCountry: "searchCountry",
      selectCountries: "selectCountries",
      showCountry: "showCountry",
      setOptionsDropDowns: "setOptionsDropDowns",
      setTextDropDowns: "setTextDropDowns"
    },
    optionsSearch: {
      showAll: "Show All",
      favorites: "Favorites",
      withoutRegion: "Without region"
    }
  },
  messageNoFound: "No results found"
}
